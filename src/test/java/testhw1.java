import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class testhw1 {
    /*
        Task 1
        Go to https://www.google.com
        Set ‘Selenium’ into search field
        Click ‘Search’ button
        In search result find link ‘https://www.selenium.dev/’ and click on it
        Check that we are on the expected page
     */
    @Test
    void test1() {
        WebDriver webDriver = new ChromeDriver();
        try {
            //Go to https://www.google.com
            webDriver.get("https://www.google.com");

            //Set ‘Selenium’ into search field
            WebElement box = webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1) > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input"));
            box.sendKeys("Selenium");

            //Click ‘Search’ button
            WebElement go = webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1) > div.A8SBwf > div.FPdoLc.tfB0Bf > center > input.gNO89b"));
            go.click();

            WebDriverWait wait1 = new WebDriverWait(webDriver, 15);
            wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(" //*[@id=\"rso\"]/div[1]/div/div/div/div[1]/a/h3")));

            //In search result find link ‘https://www.selenium.dev/’ and click on it
            WebElement link = webDriver.findElement(By.xpath("//*[@id=\"rso\"]/div[1]/div/div/div/div[1]/a/h3"));
            link.click();

            //Check that we are on the expected page
            String actualLink = webDriver.getCurrentUrl();
            String expectedLink = "https://www.selenium.dev/";
            Assertions.assertEquals(expectedLink, actualLink);
        } finally {
            webDriver.close();
        }
    }


    /*
        Task 2
        Go to mail server
        Login with valid credential
        Verify you are logged in
    */

    @Test
    void test2() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        try {
            webDriver.get("https://mail.ru/");

            WebElement email = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[1]/div[2]/input"));
            email.sendKeys("testngrtest");

            WebElement loginButton = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/button[1]"));
            loginButton.click();

            WebDriverWait wait1 = new WebDriverWait(webDriver, 10);
            wait1.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input")));

            WebElement password = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[2]/input"));
            password.sendKeys("homeworkhomework");

            WebElement loginButton2 = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/button[2]"));
            loginButton2.click();

            String expected = "testngrtest@mail.ru";

            WebDriverWait wait2 = new WebDriverWait(webDriver, 10);
            wait2.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#PH_user-email")));

            WebElement actual = webDriver.findElement(By.cssSelector("#PH_user-email"));

            Assertions.assertEquals(expected, actual.getText());

        } finally {
            webDriver.close();
        }
    }

    /*
        Task 3
        Go to mail server
        Login with invalid credential
        Check error message appears and is equal expected one
     */
    @Test
    void test3() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        try {
            webDriver.get("https://mail.ru/");

            WebElement email = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[1]/div[2]/input"));
            email.sendKeys("testngrtest");

            WebElement loginButton = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/button[1]"));
            loginButton.click();

            WebDriverWait wait1 = new WebDriverWait(webDriver, 10);
            wait1.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input")));

            WebElement password = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[2]/input"));
            password.sendKeys("homework");

            WebElement loginButton2 = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/button[2]"));
            loginButton2.click();

            WebDriverWait errorText = new WebDriverWait(webDriver, 10);
            wait1.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.error.svelte-1eyrl7y")));


            WebElement actual = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[3]"));
            Assertions.assertTrue(actual.isDisplayed());


        } finally {
            webDriver.close();
        }
    }

    /*
        Task 4
        Go to mail server+
        Login with valid credential
        Create an email
        Attach any file
        Send it to the proper recipient
    */
    @Test
    void test4() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        File file = new File("src\\test\\resources\\fileToSend.txt");
        String expected = "testngrtest@mail.ru";

        try {
            webDriver.get("https://mail.ru/");

            //login with valid credentials
            WebElement email = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[1]/div[2]/input"));
            email.sendKeys("testngrtest");

            WebElement loginButton = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/button[1]"));
            loginButton.click();

            WebDriverWait wait1 = new WebDriverWait(webDriver, 10);
            wait1.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input")));

            WebElement password = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[2]/input"));
            password.sendKeys("homeworkhomework");

            WebElement loginButton2 = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/button[2]"));
            loginButton2.click();
            webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

            //Create an email

            WebElement newEmail = webDriver.findElement(By.cssSelector("#app-canvas > div > div.application-mail > div.application-mail__overlay > " +
                    "div > div.application-mail__layout.application-mail__layout_main > span > div.layout__column.layout__column_left > div.layout__column-wrapper > div > div > div > div:nth-child(1) > div > div > a > span"));
            newEmail.click();

            webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

            WebElement mailTo = webDriver.findElement(By.className("container--zU301"));
            mailTo.sendKeys("nazringr@gmail.com");

            webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

            WebElement file1 = webDriver.findElement(By.className("desktopInput--3cWPE"));
            file1.sendKeys(file.getCanonicalPath());

            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            WebElement send = webDriver.findElement(By.xpath("/html/body/div[15]/div[2]/div/div[2]/div[1]/span[1]/span/span"));
            send.click();

            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            Assertions.assertTrue(webDriver.findElement(By.xpath("/html/body/div[9]/div/div/div[2]/div[2]/div/div/div[2]/a")).isDisplayed()); // verification of successful operation

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            webDriver.close();
        }
    }

    /*
                    Task 5
        Go to https://www.etsy.com/
        Go to submenu Clothing & Shoes -> Men's -> Boots
        Calculate all goods with discount (see photo)



        Go to https://www.etsy.com/
        Make visible submenu Clothing & Shoes -> Men's -> Boots
        Calculate links number in ‘Men’s Shoes’ section

*/

    @Test
    void test5() {
        WebDriver webDriver = new ChromeDriver();
        Actions move = new Actions(webDriver);

        try {
            webDriver.get("https://www.etsy.com/");

            WebElement clothingAndShoes = webDriver.findElement(By.cssSelector("#catnav-primary-link-10923"));

            WebDriverWait wait1 = new WebDriverWait(webDriver, 10);
            wait1.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#catnav-primary-link-10923")));

            move.moveToElement(clothingAndShoes).perform();

            WebElement men = webDriver.findElement(By.cssSelector("#side-nav-category-link-10936"));

            wait1.until(ExpectedConditions.visibilityOf(men));

            move.moveToElement(men).perform();


            WebElement boots = webDriver.findElement(By.cssSelector("#catnav-l4-11109"));
            wait1.until(ExpectedConditions.visibilityOf(boots));

            move.moveToElement(boots).click(boots).perform();


            WebElement sale = webDriver.findElement(By.cssSelector("#search-filter-reset-form > div:nth-child(2) > fieldset > div > div > div:nth-child(2) > div > a > label\n"));
            wait1.until(ExpectedConditions.visibilityOf(sale));
            sale.click();


            WebElement discount = webDriver.findElement(By.xpath("//*[@id=\"content\"]/div/div[1]/div/div[1]/div/span/span/span[2]"));
            Assertions.assertTrue(discount.isDisplayed());

//          show
            String discountDisplay = discount.getText();
            discountDisplay = discountDisplay.replaceAll("\\D+", "");
            System.out.println("The number of products on sale is    " + discountDisplay);

        } finally {
            webDriver.close();
        }
    }
}


